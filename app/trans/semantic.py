import copy

default_reg = 32
leaves2drop = ['razd_tz', ]


def parse_semantic(leave, tech_info):
    children = leave.get_children()
    new_children = []
    while new_children != children:
        children = leave.get_children()
        new_children = []
        for child in children:
            if child.name == leave.name:
                for child_child in child.get_children():
                    new_children.append(child_child)
            elif child.name not in leaves2drop:
                new_children.append(child)
        leave.clear_children()
        for new_child in new_children:
            leave.add_child(new_child)

    for child in children:
        if child.name == 'set_type':
            set_type_check(child, tech_info)
        elif child.name == 'ariph_func':
            ariph_func_check(child, tech_info)
        elif child.name == 'id':
            id_check(child, tech_info)
        parse_semantic(child, tech_info)


def id_check(leave, tech_info):
    if 'vars' not in tech_info or leave.value not in tech_info['vars']:
        raise TypeError('Переменная %s не объявлена.' % leave.value)


def set_type_check(leave, tech_info):
    if 'vars' not in tech_info:
        tech_info['vars'] = {}

    children = leave.get_children()
    variables = []
    leave_type = None
    for child in children:
        if child.name == 'type':
            leave_type = child.get_children()
        elif child.name == 'id':
            variables.append(child.value)

    var_type = leave_type[0].name
    var_reg = leave_type[2].value if var_type == 'discret' else default_reg
    for var in variables:
        if var not in tech_info['vars']:
            tech_info['vars'][var] = {'type': var_type,
                                      'reg': var_reg}
        else:
            raise TypeError('Переменная %s объявлена несколько раз.' % var)

def ariph_func_check(leave, tech_info):
    children = leave.get_children()

    var_name = children[0].value

    if 'vars' not in tech_info or var_name not in tech_info['vars']:
        raise TypeError('Переменная %s не объявлена.' % var_name)

    math_children = children[2].get_children()

    math_size = len(math_children)

    if math_size == 1:
        operand = math_children[0].get_children()[0]
        if operand.name == 'digit' and tech_info['vars'][var_name]['type'] == 'discret':
            if len(str(operand.value).replace('1', '').replace('0', '')) != 0:
                raise ValueError('Переменная %s двоичная.' % var_name)
            else:
                operand.value = int(str(operand.value), 2)

