import os
import re

productions_separator = '->'
terminals_separator = '<-'
token_or_separator = '|'

symbols2drop = [' ', '\n']

special_symbols = ['*', '(', ')']
special_symbols_pattern = '(\*|\(|\))'

special_star = '*'


class DuplicateAttention(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return 'Внимание: обнаружено дублирующееся имя %s.' % self.value


class GroupError(Exception):
    def __str__(self):
        return 'Отсутствует закрывающая/открывающия скобка.'


class SymbolsError(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return 'Неправильная последовательность символов: %s' % self.value


class NotHead(Exception):
    def __init__(self):
        pass


class ManyHeads(Exception):
    def __str__(self):
        return 'У грамматики несколько начальных нетерминалов!'


class TemplateTree:
    def __init__(self):
        self.head = None

    def __str__(self):
        stack = []
        self.head.print_generator(stack, 0, [])
        return '\n'.join(stack)

    def find_head_leave(self, production_dict):
        names_stack = {}
        for key in production_dict:
            production_dict[key].get_all_leaves_names(names_stack)
        for key in production_dict:
            if names_stack[key] == 1:
                if self.head:
                    raise ManyHeads
                self.head = production_dict[key]

    def parse_productions_dict(self, productions_dict):
        self.find_head_leave(productions_dict)
        self.find_and_paste_children(self.head, productions_dict)

    def find_and_paste_children(self, leave, productions_dict, parents_way=None):
        if not parents_way:
            parents_way = []
        if leave.name not in ['leave', 'repeater']:
            parents_way.append(leave)
        parents_way_names = [part.name for part in parents_way]
        if leave.name != 'repeater':
            leave_children = leave.get_children()
            for i in range(len(leave_children)):
                if leave_children[i].name not in ['leave', 'repeater']:
                    if leave_children[i].name in parents_way_names:
                        leave_children[i] = parents_way[parents_way_names.index(leave_children[i].name)]
                    elif leave_children[i].name in productions_dict and not leave_children[i].has_children():
                        leave_children[i] = productions_dict[leave_children[i].name]

                if leave_children[i].name != 'repeater' \
                        and leave_children[i].name not in parents_way_names \
                        and leave_children[i].need2parse:
                    self.find_and_paste_children(leave_children[i], productions_dict, parents_way)
                    leave_children[i].need2parse = False
            leave.repeater_generator()
        if leave.name not in ['leave', 'repeater']:
            parents_way.pop()


class Leave:
    def __init__(self, name, leave_type, value=None):
        self.name = name
        self.leave_type = leave_type
        self.value = value
        self._children = []

    def get_children(self):
        return self._children

    def clear_children(self):
        self._children = []

    def has_children(self):
        if self._children:
            answer = True
        else:
            answer = False
        return answer

    def add_child(self, leave):
        if isinstance(leave, Leave):
            self._children.append(leave)
        return leave

    def print_generator(self, stack, level, parents_way):
        if self in parents_way and self.name not in ['leave']:
            recursion_symbol = ' ... recursion'
        else:
            recursion_symbol = ''
        value = f', {self.value}' if self.leave_type == 'terminal' else ''
        stack.append('|' + ''.join(['____' for i in range(level)])
                     + f'<{self.name}{value}> - {self.leave_type}{recursion_symbol}')

        if self not in parents_way or self.name in ['leave']:
            parents_way.append(self)
            for child in self._children:
                child.print_generator(stack, level + 1, parents_way)
            parents_way.pop()

    def __str__(self):
        value = f', {self.value}' if self.leave_type == 'terminal' else ''
        return f'<{self.name}{value}> - {self.leave_type}'

    def __unicode__(self):
        value = f', {self.value}' if self.leave_type == 'terminal' else ''
        return f'<{self.name}{value}> - {self.leave_type}'


class TemplateLeave(Leave):
    def __init__(self, name, leave_type, value=None):
        Leave.__init__(self, name, leave_type, value)
        self.need2parse = True

    def correct_types(self, terminal_list):
        if self.name != 'repeater':
            if self.name in terminal_list and self.leave_type == 'production':
                self.leave_type = 'terminal'
            for child in self._children:
                child.correct_types(terminal_list)

    def repeater_generator(self):
        leaves_names = [leave.name for leave in self._children]
        if 'repeater' in leaves_names:
            for i in range(leaves_names.count('repeater')):
                curr_leaves_names = [leave.name for leave in self._children]
                repeater_index = curr_leaves_names.index('repeater')
                repeater_size = self._children[repeater_index].value
                repeater_index_start = repeater_index - repeater_size
                repeater = self._children[repeater_index]
                for j in range(repeater_size):
                    element = self._children.pop(repeater_index_start)
                    repeater.add_child(element)
                repeater.value = id(repeater)

    def get_all_leaves_names(self, names_dict):
        if self.name != 'repeater':
            if self.name not in names_dict:
                names_dict[self.name] = 1
            else:
                names_dict[self.name] += 1
            for child in self._children:
                child.get_all_leaves_names(names_dict)

    def print_generator(self, stack, level, parents_way):
        if self.name in parents_way and self.name not in ['leave']:
            recursion_symbol = ' ... recursion'
        else:
            recursion_symbol = ''
        value = f', {self.value}' if self.leave_type == 'terminal' else ''
        stack.append('|' + ''.join(['____' for i in range(level)])
                     + f'<{self.name}{value}> - {self.leave_type}{recursion_symbol}')

        if self.name not in parents_way or self.name in ['leave']:
            parents_way.append(self.name)
            for child in self._children:
                child.print_generator(stack, level + 1, parents_way)
            parents_way.pop()


def clear_line(line):
    for symbol in symbols2drop:
        line = line.replace(symbol, '')
    return line


def iterate_brace_counter(counter):
    if counter:
        for i in range(len(counter)):
            counter[i] += 1


def correct_types(productions_dict, terminals_list):
    for production in productions_dict:
        productions_dict[production].correct_types(terminals_list)


def parse_productions(line):
    splited_stack = []
    for part in line.split('<'):
        splited_stack.extend(part.split('>'))

    bracer_counters = []
    for part in splited_stack:
        if re.search(special_symbols_pattern, part):
            if '(' in part and ('*' in part or ')' in part):
                raise SymbolsError(part)

            previous_symbol = ''
            for symbol in part:
                if symbol == '(':
                    bracer_counters.append(0)
                elif symbol == ')':
                    if previous_symbol == ')':
                        bracer_counters.pop()
                elif symbol == '*':
                    if previous_symbol == ')':
                        repeat_leave = TemplateLeave(name='repeater',
                                                     leave_type='repeater',
                                                     value=bracer_counters.pop())

                        yield repeat_leave
                    elif not previous_symbol:
                        repeat_leave = TemplateLeave(name='repeater',
                                                     leave_type='repeater',
                                                     value=1)
                        yield repeat_leave
                    else:
                        raise SymbolsError(part)
                    iterate_brace_counter(bracer_counters)

                previous_symbol = symbol
        elif part:
            yield TemplateLeave(leave_type='production',
                                name=part)
            iterate_brace_counter(bracer_counters)
    if bracer_counters:
        raise SymbolsError('(')

def parse_file(template_path):
    terminals_dict = {}
    productions_leaves_dict = {}
    for line in open(os.path.join(*template_path), 'r'):
        line = clear_line(line)
        if productions_separator in line:
            head, body = line.split(productions_separator)
            if head in terminals_dict or head in productions_leaves_dict:
                raise DuplicateAttention(head)
            head = re.sub('(<|>)', '', head)
            new_leave = TemplateLeave(name=head,
                                      leave_type='production')
            if body.count(token_or_separator):
                for production in body.split(token_or_separator):
                    new_leave_child = new_leave.add_child(TemplateLeave(name='leave',
                                                                        leave_type='or'))
                    for leave in parse_productions(production):
                        new_leave_child.add_child(leave=leave)
            else:
                for leave in parse_productions(body):
                    new_leave.add_child(leave=leave)
            productions_leaves_dict[head] = new_leave
        elif terminals_separator in line:
            head, body = line.split(terminals_separator)
            if head in terminals_dict or head in productions_leaves_dict:
                raise DuplicateAttention(head)
            head = re.sub('(<|>)', '', head)
            terminals_dict[head] = re.compile(body)

    correct_types(productions_dict=productions_leaves_dict,
                  terminals_list=list(terminals_dict))

    grammar_tree = TemplateTree()
    grammar_tree.parse_productions_dict(productions_leaves_dict)

    return terminals_dict, grammar_tree
