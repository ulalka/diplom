from app.trans.template_parser import Leave
import copy

tree_leave_production_names = ['production', ]
tree_leave_repeater_names = ['repeater', ]
tree_leave_or_names = ['or', ]
tree_leave_terminal_names = ['terminal', ]

global_id = {}

leaves_dict = {}


class StopCompare(Exception):
    pass


class ProgramTree:
    def __init__(self):
        self.head = None

    def build_tree(self, template_tree, tokens_list):
        self.head = Leave(name=template_tree.head.name,
                          leave_type=template_tree.head.leave_type)

        for child in parse_leave(template_tree.head, tokens_list):
            self.head.add_child(child)

    def __str__(self):
        stack = []
        self.head.print_generator(stack, 0, [])
        return '\n'.join(stack)


def parse_production(leave, tokens_list, out_stack, parents_way=None):
    if not parents_way:
        parents_way = []

    tokens_list_copy = tokens_list[:]

    leaves_stack = [generated_leave for generated_leave in parse_leave(leave=leave,
                                                                       tokens_list=tokens_list_copy,
                                                                       parents_way=parents_way)]
    if leaves_stack:
        while len(tokens_list) != len(tokens_list_copy):
            tokens_list.pop(0)

        current_leave = Leave(name=leave.name,
                              leave_type=leave.leave_type)

        for new_leave in leaves_stack:
            current_leave.add_child(new_leave)

        out_stack.append(current_leave)
    else:
        raise StopCompare


def parse_terminal(leave, tokens_list, out_stack):
    if leave.name == tokens_list[0].name:
        terminal = tokens_list.pop(0)
        out_stack.append(terminal)
    else:
        raise StopCompare


def parse_or(leave, tokens_list, parents_way=None):
    if not parents_way:
        parents_way = []

    tokens_list_copy = tokens_list[:]

    leaves_stack = [generated_leave for generated_leave in parse_leave(leave=leave,
                                                                       tokens_list=tokens_list_copy,
                                                                       parents_way=parents_way)]

    if leaves_stack:
        leaves_len = len(tokens_list) - len(tokens_list_copy)
    else:
        leaves_len = 0

    return leaves_len, leaves_stack


def parse_recursion(leave, tokens_list):
    find_all_recursions(leave)
    for rec_leave in leaves_dict:
        make_recursion_variants(rec_leave, len(tokens_list))

    potential_variants = {}

    non_recursion_variants = leaves_dict[leave]['good_variants'] = copy.copy(leaves_dict[leave]['nonrec'])

    old_variants = copy.copy(non_recursion_variants)
    while non_recursion_variants:
        non_recursion_variants.extend(generate_new_combinations(leave, tokens_list))

        null_leave_stack = []
        for test_leave in non_recursion_variants:
            tokens_list_copy = tokens_list[:]
            leaves_stack = [generated_leave for generated_leave in parse_leave(leave=test_leave,
                                                                               tokens_list=tokens_list_copy)]
            if not leaves_stack:
                null_leave_stack.append(test_leave)
            else:
                potential_variants[len(tokens_list) - len(tokens_list_copy)] = leaves_stack

        for null_leave in null_leave_stack:
            if null_leave not in leaves_dict[leave]['nonrec']:
                non_recursion_variants.pop(non_recursion_variants.index(null_leave))

        if old_variants == non_recursion_variants:
            break
        else:
            old_variants = copy.copy(non_recursion_variants)

    if potential_variants:
        to_out = max_len = max(potential_variants)

        while max_len:
            tokens_list.pop(0)

            max_len -= 1
        out = potential_variants[to_out]
    else:
        out = None
    return out


def check_variants(array, tokens_list):
    drop_this = []
    for element in array:
        tokens_list_copy = tokens_list[:]
        leaves_stack = [generated_leave for generated_leave in parse_leave(leave=element,
                                                                           tokens_list=tokens_list_copy)]
        if not leaves_stack:
            drop_this.append(element)

    for element in drop_this:
        array.pop(array.index(element))


def calc_branch_size(leave):
    count = 0
    for child in leave.get_children():
        if child.leave_type in tree_leave_terminal_names:
            count += 1
        else:
            count += calc_branch_size(child)
    return count


def comb_leaves(leave, array):
    new_array = []
    leave_obj = global_id[leave.value]
    if not array:
        for leaf in leaves_dict[leave_obj]['good_variants']:
            new_array.append([leaf, ])
    else:
        for leaf in leaves_dict[leave_obj]['good_variants']:
            for arr_el in array:
                new_arr_el = copy.copy(arr_el)
                new_arr_el.append(leaf)
                new_array.append(new_arr_el)
    return new_array


def is_unique(leave1, leave2):
    children1 = leave1.get_children()
    children2 = leave2.get_children()

    if len(children1) == len(children2):
        answer = False
        for i in range(len(children1)):
            temp_answer = not(children1[i].name == children2[i].name and
                              children1[i].leave_type == children2[i].leave_type and
                              children2[i].value == children1[i].value)
            answer = answer | temp_answer | is_unique(children1[i], children2[i])
    elif not children1 and not children2:
        answer = not (leave1.name == leave2.name and
                      leave1.leave_type == leave2.leave_type and
                      leave1.value == leave2.value)
    else:
        answer = True

    return answer


def generate_new_combinations(leave, tokens_list):
    new_variants = []

    for variant in leaves_dict[leave]['rec']:
        leaves = insert_recursion_new_leaves(variant)
        original_leaves_children = [leaf.get_children() for leaf in leaves]

        variants_comb = []
        for leaf in leaves:
            variants_comb = comb_leaves(leaf, variants_comb)

        places_size = len(leaves)

        for var in variants_comb:
            for j in range(places_size):
                leaves[j].clear_children()
                for child in var[j].get_children():
                    leaves[j].add_child(child)
            variant_copy = copy.deepcopy(variant)

            is_un = True
            for var1 in leaves_dict[leave]['good_variants']:
                if not is_unique(variant_copy, var1):
                    is_un = False
                    break

            if calc_branch_size(variant_copy) <= len(tokens_list) and is_un:
                new_variants.append(variant_copy)

        # Чиним исходный вариант
        for i in range(places_size):
            leaves[i].clear_children()
            for child in original_leaves_children[i]:
                leaves[i].add_child(child)
    return new_variants


def convert2ss(value, ss, razr):
    ost = value
    stack = []

    while not ost < ss:
        stack.append(ost//ss)
        ost = ost % ss
    stack.append(ost)

    while len(stack) < razr:
        stack.append(0)
    stack.reverse()

    return stack


def calc_variants_size(variants_count, leaves_size):
    return int(pow(variants_count, leaves_size))


def calc_variants_count(leaves):
    used_leaves = []
    ss = 0
    for leave in leaves:
        leave_obj = global_id[leave.value]
        if leave_obj not in used_leaves:
            ss += len(leaves_dict[leave_obj]['good_variants'])
            used_leaves.append(leave_obj)
    return ss


def insert_recursion_new_leaves(leave):
    children = leave.get_children()
    sort_rec_dict = []
    for child in children:
        leave_obj = global_id[child.value] if child.value in global_id else None
        if leave_obj in leaves_dict:
            sort_rec_dict.append(child)
        else:
            sort_rec_dict.extend(insert_recursion_new_leaves(child))
    return sort_rec_dict


def find_all_recursions(leave, parents_way=None):
    if not parents_way:
        parents_way = []

    parents_way.append(leave)

    children = leave.get_children()

    for child in children:
        if child in parents_way:
            if child not in leaves_dict:
                leaves_dict[child] = {}
                child.value = id(child)
        else:
            find_all_recursions(child, parents_way)

    parents_way.pop()


def make_recursion_variants(leave, tokens_line_size):
    if not leaves_dict[leave]:
        leave_value = leaves_dict[leave]
        leave_value['nonrec'] = []
        leave_value['rec'] = []
        for variant, is_rec in build_recursion_branch(leave, leave, tokens_line_size):
            if is_rec and variant not in leave_value['rec']:
                variant.value = id(leave)
                leave_value['rec'].append(variant)
            else:
                leave_value['nonrec'].append(variant)
        leaves_dict[leave]['good_variants'] = copy.copy(leaves_dict[leave]['nonrec'])


def build_production_rec_branch(leave, out_stack, head, tokens_line_size):
    if leave in leaves_dict:
        if leave == head:
            new_leave = Leave(name=leave.name,
                              leave_type=leave.leave_type,
                              value=id(leave)
                              )
            global_id[id(leave)] = leave
            out_stack.append((new_leave, True))  # Нужно вытаскивать из leaves_dict все комбинации
        else:
            if not leaves_dict[leave]:
                make_recursion_variants(leave, tokens_line_size)

            rec_stack = [(new_leave, True) for new_leave in leaves_dict[leave]['rec']]
            no_rec_stack = [(new_leave, False) for new_leave in leaves_dict[leave]['nonrec']]

            out_stack.append(copy.deepcopy(rec_stack + no_rec_stack))
    else:
        temp_stack = []
        for branch, is_rec in build_recursion_branch(leave, head, tokens_line_size):
            temp_stack.append((branch, is_rec))
        if len(temp_stack) == 1:
            out_stack.append((temp_stack[0][0], temp_stack[0][1]))
        else:
            out_stack.append(temp_stack)


def build_or_rec_branch(leave, or_stack, head, tokens_line_size):
    for branch, is_rec in build_recursion_branch(leave, head, tokens_line_size):
        or_stack.append((branch, is_rec))


def build_recursion_branch(leave, head, tokens_line_size):
    children = leave.get_children()

    out_stack = []

    or_stack = []

    for child in children:
        leave_type = child.leave_type
        if leave_type in tree_leave_production_names:
            build_production_rec_branch(child, out_stack, head, tokens_line_size)
        elif leave_type in tree_leave_terminal_names:
            out_stack.append((child, False))
        elif leave_type in tree_leave_or_names:
            build_or_rec_branch(child, or_stack, head, tokens_line_size)
    if or_stack:
        out_stack.append(or_stack)

    if out_stack:
        leaves_out_stack = [[Leave(name=leave.name,
                                   leave_type=leave.leave_type,
                                   value=leave.value), False
                             ]]
        for item in out_stack:
            result = []
            if isinstance(item, list):
                for item_item, item_is_rec in item:
                    temp_leaves = copy.deepcopy(leaves_out_stack)
                    for temp_leave, is_rec in temp_leaves:
                        temp_leave.add_child(item_item)
                        result.append([temp_leave, item_is_rec | is_rec])
                leaves_out_stack = result
            else:
                item_item, item_is_rec = item
                for leave_part in leaves_out_stack:
                    leave_part[0].add_child(item_item)
                    leave_part[1] = item_is_rec | leave_part[1]

        for part in leaves_out_stack:
            yield part


def parse_repeater(leave, tokens_list, out_stack, parents_way):
    leaves_stack = [generated_leave for generated_leave in parse_leave(leave=leave,
                                                                       tokens_list=tokens_list,
                                                                       parents_way=parents_way)]
    while leaves_stack:
        for stack_leave in leaves_stack:
            out_stack.append(stack_leave)

        leaves_stack = [generated_leave for generated_leave in parse_leave(leave=leave,
                                                                           tokens_list=tokens_list,
                                                                           parents_way=parents_way)]


def parse_leave(leave, tokens_list, parents_way=None):
    if not parents_way:
        parents_way = []

    if leave.leave_type not in tree_leave_repeater_names:
        parents_way.append(leave)

    out_stack = []
    max_stack = None
    max_len = 0

    children = leave.get_children()

    for child in children:
        if not tokens_list:
            break

        child_type = child.leave_type
        if child in parents_way:
            result = parse_recursion(child, tokens_list)
            if result:
                new_leave = Leave(name=child.name,
                                  leave_type=child.leave_type,
                                  value=child.value)
                for child_res in result:
                    new_leave.add_child(child_res)
                out_stack = [new_leave, ]
                break
        elif child_type in tree_leave_repeater_names:
            parse_repeater(child, tokens_list, out_stack, parents_way)
        elif child_type in tree_leave_production_names:
            try:
                parse_production(child, tokens_list, out_stack, parents_way)
            except StopCompare:
                out_stack = []
                break
        elif child_type in tree_leave_terminal_names:
            try:
                parse_terminal(child, tokens_list, out_stack)
            except StopCompare:
                out_stack = []
                break
        elif child_type in tree_leave_or_names:
            temp_len, temp_stack = parse_or(child, tokens_list, parents_way)
            if temp_len > max_len:
                max_stack = temp_stack
                max_len = temp_len

    if max_stack:
        for i in range(max_len):
            tokens_list.pop(0)

        for max_leave in max_stack:
            yield max_leave
    else:
        for out_leave in out_stack:
            yield out_leave

    if leave.leave_type not in tree_leave_repeater_names:
        parents_way.pop()
