from app.trans.template_parser import Leave


terminals_file_symbol = ' ; '
spaces = [' ', '\n']


class TokenMatchingError(Exception):
    def __init__(self, data, line_number):
        self.value = data[:20]
        self.line_number = line_number + 1

    def __str__(self):
        return 'В коде присутствуют запрещенные символы.\n%d строка:\n"%s"' % (self.line_number, self.value)


def clear_spaces(data):
    lines_counter = 0
    while data[:1] in spaces:
        if data[0] == '\n':
            lines_counter += 1
        data = data[1:]
    return data, lines_counter


def error_finder(text, line_number):
    text = text.split('\n')
    return text[line_number]


def gen_tokens_line(data, terminals_dict):
    tokens_list = []
    lines_counter = 0
    lines2parse = data
    while len(lines2parse):
        lines2parse, temp_lines_counter = clear_spaces(lines2parse)
        if not lines2parse:
            break
        lines_counter += temp_lines_counter
        stack = []
        for terminal in terminals_dict:
            temp = terminals_dict[terminal].match(lines2parse)
            if temp:
                stack.append(Leave(name=terminal,
                                   value=temp.group(0),
                                   leave_type='terminal'))

        if not stack:
            raise TokenMatchingError(error_finder(data, lines_counter), lines_counter)

        token_element = None
        size = 0
        for element in stack:
            element_size = len(element.value)
            if element_size > size:
                token_element = element
                size = element_size

        tokens_list.append(token_element)
        lines2parse = lines2parse[size:]
    return tokens_list
