import os


def gen_signal(name, value, signal_type, reg):
    if signal_type == 'analog':
        value = str(value)
    elif signal_type == 'discret':
        value = bin(value)[2:]
        out = []
        while len(value) + len(out) < reg:
            out.append('0')
        for i in value:
            out.append(i)
        value = ''.join(out)

    while True:
        try:
            with open(os.path.join('results', 'signals', f'{name}.bin'), 'w') as file:
                file.write(value)
            break
        except Exception as e:
            continue


def read_signal(name, signal_type, reg):
    while True:
        try:
            with open(os.path.join('results', 'signals', f'{name}.bin'), 'r') as file:
                value = file.read()
            break
        except Exception as e:
            continue
    if signal_type == 'analog':
        value = float(value)
    elif signal_type == 'discret':
        value = int(value, 2)
    return value
