from os import path


class GrammarError(Exception):
    def __init__(self, leave):
        self.value = leave

    def __str__(self):
        return f'Ошибка в грамматике: около {self.value.name}'

space = '    '
api_module_name = 'app.trans.api_functions'
api_gen_signal = f'{api_module_name}.gen_signal'
api_read_signal = f'{api_module_name}.read_signal'
change_sleep_time = 1


def preset():
    stack = ['import time', 'import math', f'import {api_module_name}']

    return stack


def add_level(line, level):
    stack = [space for i in range(level)]
    stack.append(line)
    return ''.join(stack)


def gen_sleep_func(leave, tech_info):
    name, value= leave.get_children()
    value = int(value.value)
    code = [f'time.sleep({value})']
    return code


def gen_push_func(leave, tech_info):
    name, value = leave.get_children()
    code = []
    if value.name == 'id':
        value_type = tech_info['vars'][value.value]['type']
        value_reg = tech_info['vars'][value.value]['reg']
        code.append(f'{api_gen_signal}("{value.value}", {value.value}, "{value_type}", {value_reg})')
    elif value.name == 'ariph_func':
        code.extend(productions_functions[value.name](value, tech_info))
        pr_id = value.get_children()[0]
        pr_id_type = tech_info['vars'][pr_id.value]['type']
        pr_id_reg = tech_info['vars'][pr_id.value]['reg']
        code.append(f'{api_gen_signal}("{pr_id.value}", {pr_id.value}, "{pr_id_type}", {pr_id_reg})')
    return code


def gen_ariph_func(leave, tech_info):
    pr_id, pr_op, pr_value = leave.get_children()
    pr_value = ''.join(productions_functions[pr_value.name](pr_value, tech_info))
    return [f'{pr_id.value} = {pr_value}']


def gen_ariph_math(leave, tech_info):
    children = leave.get_children()
    code = []
    for child in children:
        if child.name == 'operand':
            code.append(''.join(productions_functions[child.name](child, tech_info)))
        elif child.name in ['math_op', 'left_sb', 'right_sb']:
            code.append(child.value)
        elif child.name == 'sin':
            code.append('math.sin')
        elif child.name == 'cos':
            code.append('math.cos')
        elif child.name == 'tg':
            code.append('math.tan')
        elif child.name == 'ctg':
            code.append('1 / math.tan')
        elif child.name == 'abs':
            code.append('math.fabs')
        elif child.name == 'sqrt':
            code.append('math.sqrt')
    return code


def gen_operand(leave, tech_info):
    child, = leave.get_children()
    return [str(child.value)]


def gen_inv_func(leave, tech_info):
    task, pr_id = leave.get_children()
    return [f'{pr_id.value} = ~{pr_id.value}']


def gen_inc_func(leave, tech_info):
    name, value = leave.get_children()
    return [f'{value.value} += 1']


def gen_dec_func(leave, tech_info):
    name, value = leave.get_children()
    return [f'{value.value} -= 1']


def gen_change_func(leave, tech_info):
    children = leave.get_children()
    pr_id = children[1]
    op1, = children[4].get_children()
    op2, = children[6].get_children()
    time, = children[9].get_children()
    pr_id_type = tech_info['vars'][pr_id.value]['type']
    pr_id_reg = tech_info['vars'][pr_id.value]['reg']
    code = [f'{pr_id.value} = {op1.value}',
            f'_{pr_id.value}_step = ({op2.value} - {op1.value}) / {time.value}',
            f'while {pr_id.value} != {op2.value}:',
            add_level(f'time.sleep({change_sleep_time})', 1),
            add_level(f'{pr_id.value} += _{pr_id.value}_step', 1),
            add_level(f'{api_gen_signal}("{pr_id.value}", {pr_id.value}, "{pr_id_type}", {pr_id_reg})', 1)]
    return code


def gen_expr(leave, tech_info):
    code = []
    for child in leave.get_children():
        if child.name == 'operand':
            code.extend(productions_functions[child.name](child, tech_info))
        elif child.name in ['expr_symb', 'left_sb', 'right_sb']:
            code.append(child.value)
        elif child.name == 'expr_or_and':
            if child.value == '&&':
                code.append(' and ')
            elif child.value == '||':
                code.append(' or ')
    return code


def gen_expr_operand(leave, tech_info):
    child, = leave.get_children()
    if child.name == 'bin_digit':
        code = [f'int({child.value}, 2)']
    else:
        code = [child.value]
    return code


def gen_if_func(leave, tech_info):
    children = leave.get_children()
    expr = children[2]
    stmt = children[5]
    code = ['if ' + ''.join(productions_functions[expr.name](expr, tech_info)) + ':']
    for line in parse_body_stmt(stmt, tech_info):
        code.append(add_level(line, 1))
    return code


def gen_while_func(leave, tech_info):
    children = leave.get_children()
    expr = children[2]
    stmt = children[5]
    code = ['while ' + ''.join(productions_functions[expr.name](expr, tech_info)) + ':']
    for line in parse_body_stmt(stmt, tech_info):
        code.append(add_level(line, 1))
    return code


def gen_read_func(leave, tech_info):
    pr_id = leave.get_children()[1]
    pr_id_type = tech_info['vars'][pr_id.value]['type']
    pr_id_reg = tech_info['vars'][pr_id.value]['reg']
    return [f'{pr_id.value} = {api_read_signal}({pr_id.value}, "{pr_id_type}", {pr_id_reg})']


def gen_set_type(leave, tech_info):
    code = []
    for child in leave.get_children():
        if child.name == 'id':
            code.append('%s = ' % child.value)
    code.append('0')
    return [''.join(code)]


productions_functions = {'sleep_func': gen_sleep_func,
                         'push_func': gen_push_func,
                         'ariph_func': gen_ariph_func,
                         'ariph_math': gen_ariph_math,
                         'operand': gen_operand,
                         'inv_func': gen_inv_func,
                         'inc_func': gen_inc_func,
                         'dec_func': gen_dec_func,
                         'change_func': gen_change_func,
                         'expr': gen_expr,
                         'if_func': gen_if_func,
                         'while_func': gen_while_func,
                         'read_func': gen_read_func,
                         'set_type': gen_set_type,

                         }


def parse_body_stmt(leave, tech_info):
    code_stack = []
    for child in leave.get_children():
        if child.name == 'stmt':
            for child_of_child in child.get_children():
                if child_of_child.name in productions_functions:
                    code_stack.extend(productions_functions[child_of_child.name](child_of_child, tech_info))
    return code_stack


def generate_code(tree, program_path, tech_info):
    code_stack = []
    code_stack.extend(preset())

    code_stack.append('try:')
    try_stack = []
    for line in parse_body_stmt(tree.head, tech_info):
        try_stack.append(add_level(line, 1))
    if try_stack:
        code_stack.extend(try_stack)
    else:
        code_stack.append(add_level('pass',1))
    code_stack.append('except Exception as e:')
    code_stack.append(add_level('print(e)', 1))

    with open(path.join(*program_path), 'w') as file:
        file.write('\n'.join(code_stack))
