import app.trans.lexer as lexer
from app.trans.template_parser import parse_file as parse_grammar
import app.trans.parser as parser
import app.trans.codegen as codegen
import app.trans.semantic as semantic
import os
grammar_path = ['app', 'trans', 'grammar.txt']
program_path = ['results', 'program.py']


def create_program(text):
    terminals_dict, grammar_tree = parse_grammar(grammar_path)

    tokens_line = lexer.gen_tokens_line(text, terminals_dict)

    program_tree = parser.ProgramTree()
    program_tree.build_tree(template_tree=grammar_tree,
                            tokens_list=tokens_line)
    tech_info = {}
    semantic.parse_semantic(program_tree.head, tech_info)

    codegen.generate_code(tree=program_tree,
                          program_path=program_path,
                          tech_info=tech_info)


def get_grammar():
    grammar_text = open(os.path.join(*grammar_path), 'r').read()
    terminals_dict, grammar_tree = parse_grammar(grammar_path)
    return grammar_text, str(grammar_tree)

def get_lexer(text):
    pass
