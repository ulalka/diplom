import multiprocessing as mp
import subprocess
import sys, os, time


class Subproc:
    def __init__(self):
        self._process = None
        self.from_process = mp.Queue()
        self.to_process = mp.Queue()

    def create_process(self):
        self._process = subprocess.Popen([sys.executable,
                                          os.path.join('results', 'program.py')],
                                         stderr=subprocess.PIPE,
                                         stdout=subprocess.PIPE)
        msg, out = self._process.communicate()
        return msg.decode()

    def kill_process(self):
        self._process.kill()