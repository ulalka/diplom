from flask import render_template
from app import app
import app.worker as worker
from flask import request, json
from app import subproc


@app.route('/')
@app.route('/index/', methods=['GET', 'POST'])
def index():
    return render_template('index.html',
                           title='Эмулятор работы технологических объектов')


@app.route('/grammar/', methods=['GET', 'POST'])
def grammar():
    grammar_code, tree_code = worker.get_grammar()
    return render_template('grammar.html',
                           title='Грамматика языка сценариев',
                           grammar_code=grammar_code,
                           tree_code=tree_code
                           )

@app.route('/handler/', methods=['POST'])
def handler():
    command = request.args.get('cmd')
    if command:
        if command == 'create-program':
            request_data = request.get_json()
            try:
                worker.create_program(request_data['body'])
            except Exception as e:
                return json.dumps({'body': str(e)}), 400
        elif command == 'start-program':
            try:
                msg = subproc.create_process()
                if msg:
                    return json.dumps({'body': str(msg)}), 400
            except Exception as e:
                return json.dumps({'body': str(e)}), 400
        elif command == 'kill-program':
            subproc.kill_process()

    return '', 200
