import app.trans.lexer as lexer
from app.trans.template_parser import parse_file as parse_grammar
import app.trans.parser as parser
import app.trans.codegen as codegen
import app.trans.semantic as semantic

import os

file_split = '================'
name_split = '~~~~~~~~~'

file_name = 'programms.txt'

parent_dir = os.path.split(os.getcwd())[0]
grammar_path = [parent_dir, 'trans', 'grammar.txt']

program_path = [os.path.split(parent_dir)[0], 'results', 'program.py']

terminals_dict_etalon = r"{'digit': re.compile('[0-9]+(\\.[0-9]+)?\\b'), 'sleep': re.compile('SLEEP\\b'), 'push': re.compile('PUSH\\b'), 'read': re.compile('READ\\b'), 'dec': re.compile('DEC\\b'), 'inc': re.compile('INC\\b'), 'if': re.compile('IF\\b'), 'while': re.compile('WHILE\\b'), 'analog': re.compile('ANALOG\\b'), 'discret': re.compile('DISCRET\\b'), 'change': re.compile('CHANGE\\b'), 'in': re.compile('IN\\b'), 'for': re.compile('FOR\\b'), 'with': re.compile('WITH\\b'), 'inv': re.compile('INV\\b'), 'sin': re.compile('SIN\\b'), 'cos': re.compile('COS\\b'), 'tg': re.compile('TAN\\b'), 'ctg': re.compile('CTAN\\b'), 'sqrt': re.compile('SQRT\\b'), 'abs': re.compile('ABS\\b'), 'id': re.compile('[a-zA-Z][_a-zA-Z0-9]*\\b'), 'set_op': re.compile('='), 'math_op': re.compile('(\\+|\\-|\\^|\\/|\\%|\\*|\\\\|\\&|\\||\\В«|\\В»)'), 'expr_or_and': re.compile('(\\&\\&|\\|\\|)'), 'expr_symb': re.compile('(\\<\\=|\\>\\=|\\<|\\>|\\=\\=|\\!\\=)'), 'group_symb_left': re.compile('\\['), 'group_symb_right': re.compile('\\]'), 'razd_tz': re.compile(';'), 'razd_z': re.compile(','), 'razd_dt': re.compile(':'), 'left_sb': re.compile('\\('), 'right_sb': re.compile('\\)'), 'left_fig': re.compile('\\{'), 'right_fig': re.compile('\\}')}"

grammar_tree_etalon = """
|<body> - production
|____<repeater> - repeater
|________<stmt> - production
|____________<leave> - or
|________________<while_func> - production
|____________________<while, None> - terminal
|____________________<left_sb, None> - terminal
|____________________<expr> - production
|________________________<leave> - or
|____________________________<operand> - production
|________________________________<leave> - or
|____________________________________<id, None> - terminal
|________________________________<leave> - or
|____________________________________<digit, None> - terminal
|____________________________<expr_symb, None> - terminal
|____________________________<operand> - production
|________________________________<leave> - or
|____________________________________<id, None> - terminal
|________________________________<leave> - or
|____________________________________<digit, None> - terminal
|________________________<leave> - or
|____________________________<expr> - production ... recursion
|____________________________<expr_or_and, None> - terminal
|____________________________<expr> - production ... recursion
|____________________<right_sb, None> - terminal
|____________________<left_fig, None> - terminal
|____________________<while_body> - production
|________________________<repeater> - repeater ... recursion
|____________________<right_fig, None> - terminal
|____________<leave> - or
|________________<set_type> - production
|____________________<leave> - or
|________________________<type> - production
|____________________________<leave> - or
|________________________________<analog, None> - terminal
|____________________________<leave> - or
|________________________________<discret, None> - terminal
|________________________________<left_sb, None> - terminal
|________________________________<digit, None> - terminal
|________________________________<right_sb, None> - terminal
|________________________<id, None> - terminal
|____________________<leave> - or
|________________________<type> - production
|____________________________<leave> - or
|________________________________<analog, None> - terminal
|____________________________<leave> - or
|________________________________<discret, None> - terminal
|________________________________<left_sb, None> - terminal
|________________________________<digit, None> - terminal
|________________________________<right_sb, None> - terminal
|________________________<id, None> - terminal
|________________________<repeater> - repeater ... recursion
|________________<razd_tz, None> - terminal
|____________<leave> - or
|________________<inv_func> - production
|____________________<inv, None> - terminal
|____________________<id, None> - terminal
|________________<razd_tz, None> - terminal
|____________<leave> - or
|________________<ariph_func> - production
|____________________<id, None> - terminal
|____________________<set_op, None> - terminal
|____________________<ariph_math> - production
|________________________<leave> - or
|____________________________<operand> - production
|________________________________<leave> - or
|____________________________________<id, None> - terminal
|________________________________<leave> - or
|____________________________________<digit, None> - terminal
|________________________<leave> - or
|____________________________<ariph_math> - production ... recursion
|____________________________<math_op, None> - terminal
|____________________________<ariph_math> - production ... recursion
|________________________<leave> - or
|____________________________<sin, None> - terminal
|____________________________<left_sb, None> - terminal
|____________________________<operand> - production
|________________________________<leave> - or
|____________________________________<id, None> - terminal
|________________________________<leave> - or
|____________________________________<digit, None> - terminal
|____________________________<right_sb, None> - terminal
|________________________<leave> - or
|____________________________<cos, None> - terminal
|____________________________<left_sb, None> - terminal
|____________________________<operand> - production
|________________________________<leave> - or
|____________________________________<id, None> - terminal
|________________________________<leave> - or
|____________________________________<digit, None> - terminal
|____________________________<right_sb, None> - terminal
|________________________<leave> - or
|____________________________<tg, None> - terminal
|____________________________<left_sb, None> - terminal
|____________________________<operand> - production
|________________________________<leave> - or
|____________________________________<id, None> - terminal
|________________________________<leave> - or
|____________________________________<digit, None> - terminal
|____________________________<right_sb, None> - terminal
|________________________<leave> - or
|____________________________<ctg, None> - terminal
|____________________________<left_sb, None> - terminal
|____________________________<operand> - production
|________________________________<leave> - or
|____________________________________<id, None> - terminal
|________________________________<leave> - or
|____________________________________<digit, None> - terminal
|____________________________<right_sb, None> - terminal
|________________________<leave> - or
|____________________________<sqrt, None> - terminal
|____________________________<left_sb, None> - terminal
|____________________________<operand> - production
|________________________________<leave> - or
|____________________________________<id, None> - terminal
|________________________________<leave> - or
|____________________________________<digit, None> - terminal
|____________________________<right_sb, None> - terminal
|________________________<leave> - or
|____________________________<abs, None> - terminal
|____________________________<left_sb, None> - terminal
|____________________________<operand> - production
|________________________________<leave> - or
|____________________________________<id, None> - terminal
|________________________________<leave> - or
|____________________________________<digit, None> - terminal
|____________________________<right_sb, None> - terminal
|________________<razd_tz, None> - terminal
|____________<leave> - or
|________________<dec_func> - production
|____________________<dec, None> - terminal
|____________________<id, None> - terminal
|________________<razd_tz, None> - terminal
|____________<leave> - or
|________________<change_func> - production
|____________________<change, None> - terminal
|____________________<id, None> - terminal
|____________________<in, None> - terminal
|____________________<group_symb_left, None> - terminal
|____________________<operand> - production
|________________________<leave> - or
|____________________________<id, None> - terminal
|________________________<leave> - or
|____________________________<digit, None> - terminal
|____________________<razd_z, None> - terminal
|____________________<operand> - production
|________________________<leave> - or
|____________________________<id, None> - terminal
|________________________<leave> - or
|____________________________<digit, None> - terminal
|____________________<group_symb_right, None> - terminal
|____________________<for, None> - terminal
|____________________<operand> - production
|________________________<leave> - or
|____________________________<id, None> - terminal
|________________________<leave> - or
|____________________________<digit, None> - terminal
|________________<razd_tz, None> - terminal
|____________<leave> - or
|________________<sleep_func> - production
|____________________<sleep, None> - terminal
|____________________<digit, None> - terminal
|________________<razd_tz, None> - terminal
|____________<leave> - or
|________________<push_func> - production
|____________________<leave> - or
|________________________<push, None> - terminal
|________________________<id, None> - terminal
|____________________<leave> - or
|________________________<push, None> - terminal
|________________________<ariph_func> - production
|____________________________<id, None> - terminal
|____________________________<set_op, None> - terminal
|____________________________<ariph_math> - production
|________________________________<leave> - or
|____________________________________<operand> - production
|________________________________________<leave> - or
|____________________________________________<id, None> - terminal
|________________________________________<leave> - or
|____________________________________________<digit, None> - terminal
|________________________________<leave> - or
|____________________________________<ariph_math> - production ... recursion
|____________________________________<math_op, None> - terminal
|____________________________________<ariph_math> - production ... recursion
|________________________________<leave> - or
|____________________________________<sin, None> - terminal
|____________________________________<left_sb, None> - terminal
|____________________________________<operand> - production
|________________________________________<leave> - or
|____________________________________________<id, None> - terminal
|________________________________________<leave> - or
|____________________________________________<digit, None> - terminal
|____________________________________<right_sb, None> - terminal
|________________________________<leave> - or
|____________________________________<cos, None> - terminal
|____________________________________<left_sb, None> - terminal
|____________________________________<operand> - production
|________________________________________<leave> - or
|____________________________________________<id, None> - terminal
|________________________________________<leave> - or
|____________________________________________<digit, None> - terminal
|____________________________________<right_sb, None> - terminal
|________________________________<leave> - or
|____________________________________<tg, None> - terminal
|____________________________________<left_sb, None> - terminal
|____________________________________<operand> - production
|________________________________________<leave> - or
|____________________________________________<id, None> - terminal
|________________________________________<leave> - or
|____________________________________________<digit, None> - terminal
|____________________________________<right_sb, None> - terminal
|________________________________<leave> - or
|____________________________________<ctg, None> - terminal
|____________________________________<left_sb, None> - terminal
|____________________________________<operand> - production
|________________________________________<leave> - or
|____________________________________________<id, None> - terminal
|________________________________________<leave> - or
|____________________________________________<digit, None> - terminal
|____________________________________<right_sb, None> - terminal
|________________________________<leave> - or
|____________________________________<sqrt, None> - terminal
|____________________________________<left_sb, None> - terminal
|____________________________________<operand> - production
|________________________________________<leave> - or
|____________________________________________<id, None> - terminal
|________________________________________<leave> - or
|____________________________________________<digit, None> - terminal
|____________________________________<right_sb, None> - terminal
|________________________________<leave> - or
|____________________________________<abs, None> - terminal
|____________________________________<left_sb, None> - terminal
|____________________________________<operand> - production
|________________________________________<leave> - or
|____________________________________________<id, None> - terminal
|________________________________________<leave> - or
|____________________________________________<digit, None> - terminal
|____________________________________<right_sb, None> - terminal
|________________<razd_tz, None> - terminal
|____________<leave> - or
|________________<inc_func> - production
|____________________<inc, None> - terminal
|____________________<id, None> - terminal
|________________<razd_tz, None> - terminal
|____________<leave> - or
|________________<if_func> - production
|____________________<if, None> - terminal
|____________________<left_sb, None> - terminal
|____________________<expr> - production
|________________________<leave> - or
|____________________________<operand> - production
|________________________________<leave> - or
|____________________________________<id, None> - terminal
|________________________________<leave> - or
|____________________________________<digit, None> - terminal
|____________________________<expr_symb, None> - terminal
|____________________________<operand> - production
|________________________________<leave> - or
|____________________________________<id, None> - terminal
|________________________________<leave> - or
|____________________________________<digit, None> - terminal
|________________________<leave> - or
|____________________________<expr> - production ... recursion
|____________________________<expr_or_and, None> - terminal
|____________________________<expr> - production ... recursion
|____________________<right_sb, None> - terminal
|____________________<left_fig, None> - terminal
|____________________<if_body> - production
|________________________<repeater> - repeater ... recursion
|____________________<right_fig, None> - terminal
|____________<leave> - or
|________________<read_func> - production
|____________________<read, None> - terminal
|____________________<id, None> - terminal
|________________<razd_tz, None> - terminal
"""

def load_tests():
    tests = []
    with open(file_name, 'r') as file:
        for test in file.read().split(file_split):
            test_name, code, tokens_line, tree, super_tree, result_code = test.split(name_split)
            tests.append({'name': test_name.replace('\n', ''),
                          'code': code,
                          'tokens_line': tokens_line.replace('\n', ''),
                          'tree': tree,
                          'super_tree': super_tree,
                          'result_code': result_code
                          })
    return tests


def template_parser_test():
    print('\n=== TEMPLATE PARSER TEST ===\n')
    terminals_dict, grammar_tree = parse_grammar(grammar_path)
    if str(terminals_dict).replace('\n', '') == terminals_dict_etalon.replace('\n', ''):
        print('== TERMINALS DICT - GOOD')
    else:
        print('== TERMINALS DICT - BAD')
    if str(grammar_tree).replace('\n', '') == grammar_tree_etalon.replace('\n', ''):
        print('== GRAMMAR TREE - GOOD')
    else:
        print('== GRAMMAR TREE - BAD')



def lexer_test():
    print('\n=== LEXER TEST ===\n')
    terminals_dict, grammar_tree = parse_grammar(grammar_path)
    tests = load_tests()
    for test in tests:
        tokens_line = lexer.gen_tokens_line(test['code'], terminals_dict)
        text_out_stack = []
        for token in tokens_line:
            text_out_stack.append('<%s, "%s"> ' % (token.name, token.value))
        test_result = ''.join(text_out_stack)[:-1]
        if test_result == test['tokens_line']:
            print(f'== {test["name"]} - GOOD')
        else:
            print(f'== {test["name"]} - BAD')
            print(f'BAD: {test_result}')
            print(f'GOOD: {test["tokens_line"]}')


def parser_test():
    print('\n=== PARSER TEST ===\n')
    terminals_dict, grammar_tree = parse_grammar(grammar_path)
    tests = load_tests()

    for test in tests:
        tokens_line = lexer.gen_tokens_line(test['code'], terminals_dict)
        program_tree = parser.ProgramTree()
        program_tree.build_tree(template_tree=grammar_tree,
                                tokens_list=tokens_line)
        test_result = str(program_tree)
        test_clear = test_result.replace('\n', '')
        real_clear = test['tree'].replace('\n', '')
        if test_clear == real_clear:
            print(f'== {test["name"]} - GOOD')
        else:
            print(f'== {test["name"]} - BAD')
            print(f'BAD: {test_result}')
            print(f'GOOD: {test["tree"]}')

def semantic_test():
    print('\n=== SEMANTIC TEST ===\n')
    terminals_dict, grammar_tree = parse_grammar(grammar_path)
    tests = load_tests()

    for test in tests:
        tokens_line = lexer.gen_tokens_line(test['code'], terminals_dict)
        program_tree = parser.ProgramTree()
        program_tree.build_tree(template_tree=grammar_tree,
                                tokens_list=tokens_line)
        tech_info = {}
        semantic.parse_semantic(program_tree.head, tech_info)
        test_result = str(program_tree)
        if test_result.replace('\n', '') == test['super_tree'].replace('\n', ''):
            print(f'== {test["name"]} - GOOD')
        else:
            print(f'== {test["name"]} - BAD')
            print(f'BAD: {test_result}')
            print(f'GOOD: {test["super_tree"]}')


def codegen_test():
    print('\n=== CODE GENERATOR TEST ===\n')
    terminals_dict, grammar_tree = parse_grammar(grammar_path)
    tests = load_tests()

    for test in tests:
        tokens_line = lexer.gen_tokens_line(test['code'], terminals_dict)
        program_tree = parser.ProgramTree()
        program_tree.build_tree(template_tree=grammar_tree,
                                tokens_list=tokens_line)
        tech_info = {}
        semantic.parse_semantic(program_tree.head, tech_info)

        codegen.generate_code(program_tree, program_path, tech_info)
        with open(os.path.join(*program_path), 'r') as file:
            test_result = file.read()

        if test_result.replace('\n', '') == test['result_code'].replace('\n', ''):
            print(f'== {test["name"]} - GOOD')
        else:
            print(f'== {test["name"]} - BAD')
            print(f'BAD: {test_result}')
            print(f'GOOD: {test["result_code"]}')


if __name__ == '__main__':
    template_parser_test()
    lexer_test()
    parser_test()
    semantic_test()
    codegen_test()


