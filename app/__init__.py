from flask import Flask
from app.subprocess_worker import Subproc

app = Flask(__name__)
app.config.from_object('config')

subproc = Subproc()


from app import views

