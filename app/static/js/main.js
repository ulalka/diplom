$(document).ready(function(){
    $('#gen-button').click(function(){
        if ($('#program-code').val().length > 0){
        request = {'body': $('#program-code').val()};
        $.ajax({type: 'POST',
                url: '/handler/?cmd=create-program',
                data: JSON.stringify(request),
                contentType: 'application/json;charset=UTF-8',
                dataType: 'html',
                success: function(){
                add_message('Код сгенерирован успешно.');
                $('#start-button').show();
                $('#kill-button').show();
                },
                error: function(request){
                    request = JSON.parse(request.responseText);
                    add_message('Ошибка: ' + request['body'])
                }}
        );
        }
        else{
            add_message('Код пуст.')
        }
    });

    $('#start-button').click(function(){
        add_message('Выполнение программы.');
        $.ajax({type: 'POST',
                url: '/handler/?cmd=start-program',
                contentType: 'application/json;charset=UTF-8',
                dataType: 'html',
                success: function(request){
                    add_message('Программа успешно выполнена.')
                },
                error: function(request){
                    request = JSON.parse(request.responseText);
                    add_message('Ошибка: ' + request['body']);
        }}
        );
    });
    $('#clear').click(function(){
        $('.msg-box').empty();
    });
    $('#kill-button').click(function(){
        $.ajax({type: 'POST',
                url: '/handler/?cmd=kill-program',
                contentType: 'application/json;charset=UTF-8',
                dataType: 'html',
                success: function(request){
                request = JSON.parse(request);
                    add_message('Программа остановлена.')
                },
                error: function(request){
                    request = JSON.parse(request.responseText);
                    add_message('Ошибка: ' + request['body'])
        }}
        );
    });
});

function add_message(msg){
    var date = new Date();

    var options = {
      year: 'numeric',
      month: 'numeric',
      day: 'numeric',
      hour: 'numeric',
      minute: 'numeric',
      second: 'numeric'
    };
    send_msg = '<div class="msg-block">' + date.toLocaleString("ru", options) + '<br />' + msg + '</div>';
    $('.msg-box').prepend(send_msg);
}