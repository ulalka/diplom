import time
import math
import app.trans.api_functions
try:
    a = b = 0
    a = 1
    b = 2
    a = 2
    _a_step = (3 - 2) / b
    while a != 3:
        time.sleep(1)
        a += _a_step
        app.trans.api_functions.gen_signal("a", a, "analog", 32)
except Exception as e:
    print(e)